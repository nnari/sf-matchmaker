class Player {
  constructor (uid, main) {
    this.uid = uid;
    this.main = main;
    this.secondaries = [];
  }
  
}

module.exports = Player;