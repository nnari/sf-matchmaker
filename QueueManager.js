const { RichEmbed } = require("discord.js");
const PlayerManager = require("./PlayerManager");
const config = require("./data/config.json");
const moment = require("moment");

let queue = [];
const DEFAULT_MATCHMAKING_TIME = 30;

const StartTimers = () => {
  setInterval(() => {
      for (let i = 0; i < queue.length; i++) {
        let now = moment().toDate();
        console.log("Checking for expired queue entries")
        if(queue[i].endTime.isBefore(moment())) {
          queue[i].send(`The matchmaking session you started at ${queue[i].startTime.format("HH:mm")} has expired.`);
          queue.splice(i, 1);
          console.log("Removed expired queue entry at position", i);

        }
      }
  }, 20 * 1000); // 60 * 1000 check every minute
}

const AddPlayerToQueue = (message, time) => {
  let index = queue.find(user => user.id === message.author.id);
  try {
    time = parseInt(time) || DEFAULT_MATCHMAKING_TIME;
  } catch (err) {
    console.log("Couldn't parse the time value:", err);
  }
  if(typeof(time) === "number" && time <= 120 && time >= 5) {
    if (index) {
      message.channel.send("You have been removed from matchmaking.");
      queue.splice(index, 1);
    } else {
      let now = moment();
      message.author.startTime = now;
      message.author.endTime = moment(now).add(time, 'minutes');
      queue.push(message.author);
      MatchFound(message, time);
    }
  }
}

const MatchFound = (message, time) => {
  if(queue.length === 2) {
    let player1 = PlayerManager.FindPlayerProfile(queue[0].id) || {main: "Unknown Main"};
    let player2 = PlayerManager.FindPlayerProfile(queue[1].id) || {main: "Unknown Main"};
    message.channel.send(`Match ready! ${queue[0]} vs. ${queue[1]} on Smash Ultimate!`);
    const embed = new RichEmbed()
      .setTitle("Match info")
      .setColor("#" + Math.random().toString(16).slice(2, 8)) // LUL)
      .addField(queue[0].username, player1.main, true)
      .addField(queue[1].username, player2.main, true)
      .addBlankField()
      .addField('Secondaries', PlayerManager.BuildSecondariesText(player1.secondaries), true)
      .addField('Secondaries', PlayerManager.BuildSecondariesText(player2.secondaries), true)
      .setTimestamp()
    message.channel.send(embed);
    queue = [];
    return true;
  } else {
    message.channel.send(`Matchmaking on Smash Ultimate for ${time} minutes!`);
    return false;
  }
}

module.exports.StartTimers = StartTimers;
module.exports.AddPlayerToQueue = AddPlayerToQueue;