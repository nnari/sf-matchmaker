const Discord = require("discord.js");
const client = new Discord.Client();
const config = require("./data/config.json");
const PlayerManager = require("./PlayerManager");
const QueueManager = require("./QueueManager");
const Util = require("./util");

PlayerManager.init(client);
PlayerManager.LoadPlayers();
QueueManager.StartTimers();

client.on("ready", () => {
  console.log("Bot ready");
});

client.on("message", message => {
  //Ignore other bots and itself
  if (message.author.bot) return;
  if (message.content.indexOf(config.prefix) !== 0) return;

  //Split the arguments
  const args = message.content
    .slice(config.prefix.length)
    .trim()
    .split(/ +/g);
  const command = args.shift().toLowerCase();
  console.log(command);

  if(message.channel.id === config.channels.matchmaking) {
    switch (command) {
      case "m":
        QueueManager.AddPlayerToQueue(message, args[0]);
        break;
      case "me":
        PlayerManager.ShowCharacters(message);
        break;
      case "who":
        PlayerManager.Who(message, args[0]);
      case "register":
        PlayerManager.Register(message, PlayerManager.BuildCharacterName(args));
        break;
      case "secondary": 
        PlayerManager.ManageSecondary(message, args[0], PlayerManager.BuildCharacterName(args.slice(1)));
        break;
      case "main":
        PlayerManager.ChangeMain(message, PlayerManager.BuildCharacterName(args));
        break;
      case "help":
        Util.Help(message);
        break;
    }
  }

  if(message.channel.id === config.channels.botconfig) {
    if(command === "register") {
      PlayerManager.Register(message, PlayerManager.BuildCharacterName(args));
    }
    if(command === "secondary") {
      PlayerManager.ManageSecondary(message, args[0], PlayerManager.BuildCharacterName(args.slice(1)));
    }
    if(command === "main") {
      PlayerManager.ChangeMain(message, PlayerManager.BuildCharacterName(args));
    }
    if(command === "help") {
      Util.Help(message);
    }
  }
});

//Login to the client and enable event listeners
client.login(config.token);
